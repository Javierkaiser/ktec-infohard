const electron = require('electron');
const ipc = electron.ipcRenderer;

const $ = require('jquery');
require('popper.js');
require('bootstrap');

//Globales
let computer = null;
let interval = null;

//Recibo los datos
ipc.on('Datos', (e, datos) => {
  computer = datos;
  
  $("#gso").text(computer.resumen.gso);
  $("#gmother").text(computer.resumen.gmother);
  $("#gcpu").text(computer.resumen.gcpu);
  $("#gram").text(computer.resumen.gram);
  $("#ghdds").text(computer.resumen.ghdds);
});

function mainload()
{
  $("#footAnio").text(new Date().getFullYear());
  ipc.send("Pedido-Datos", true);
  cargar("principal");
}

function cargar(loque)
{
  $("#cuerpo").load(`../html/secciones/${loque}/${loque}.html`);
  $("a").removeClass("active");
  $("#" + loque).addClass("active");
}

function minimizar()
{
  ipc.send('Minimizo', true);
}
function maximizar()
{
  ipc.send('Maximizo', true);
}
function cerrar()
{
  ipc.send('Cierro', true);
}


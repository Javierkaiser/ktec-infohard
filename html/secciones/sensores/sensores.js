async function loadSensors() {
    const si = require('systeminformation');
    const vel = await si.cpuCurrentspeed();
    const temp = await si.cpuTemperature();

    $("#cpuAvg").text(vel.avg + ' GHz');
    $("#cpuMin").text(vel.min + ' GHz');
    $("#cpuMax").text(vel.max + ' GHz');
    $("#cpuTemp").text(temp.main + ' °C');
    $("#cpuTempMax").text(temp.max + ' °C');        
}

loadSensors();
interval = setInterval(loadSensors, 1000);

function stopSensors() {
    clearInterval(interval);
}

$(function () {
    $('[data-toggle="popover"]').popover()
})
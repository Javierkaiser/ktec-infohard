function loadMemoria() {
    if (computer !== null) {
        let html = '';
        for (const ram of computer.memoriaModulos) {
            //Memorias
            const capacidad = Math.round(ram.size / 1073741824);
            html += '<ul class="list-group list-group-flush"><li class="list-group-item text-white d-flex justify-content-between align-items-center"><label> Fabricante</label ><label>' + ram.manufacturer + '</label></li ><li class="list-group-item text-white d-flex justify-content-between align-items-center"><label>Capacidad</label><label>' + capacidad + ' GB</label></li><li class="list-group-item text-white d-flex justify-content-between align-items-center"><label>Forma</label><label>' + ram.formFactor + '</label></li><li class="list-group-item text-white d-flex justify-content-between align-items-center"><label>Banco de Memoria</label><label>' + ram.bank + '</label></li><li class="list-group-item text-white d-flex justify-content-between align-items-center"><label>Frecuencia</label><label>' + ram.clockSpeed + ' MHz</label></li><li class="list-group-item text-white d-flex justify-content-between align-items-center"><label>Número de Serie</label><label>' + ram.serialNum + '</label></li><li class="list-group-item text-white d-flex justify-content-between align-items-center"><label>Número de Parte</label><label>' + ram.partNum + '</label></li></ul><br>';
        };
        $("#memorias").html(html);
        clearInterval(interval);
    }
}

loadMemoria();
interval = setInterval(loadMemoria, 100);
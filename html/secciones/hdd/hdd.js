function loadHdds() {
    if (computer !== null) {
        let html = '';
        for (const hdd of computer.hdds) {
            html += '<ul class="list-group list-group-flush"><li class="list-group-item text-white d-flex justify-content-between align-items-center"><label>Modelo</label ><label>' + hdd.name + '</label></li ><li class="list-group-item text-white d-flex justify-content-between align-items-center"><label>Capacidad</label><label>' + hdd.capacidad + ' GB</label></li><li class="list-group-item text-white d-flex justify-content-between align-items-center"><label>Tipo de Inteface</label><label>' + hdd.interfaceType + '</label></li><li class="list-group-item text-white d-flex justify-content-between align-items-center"><label>Estado</label><label>' + hdd.smartStatus + '</label></li><li class="list-group-item text-white d-flex justify-content-between align-items-center"><label>Número de Serie</label><label>' + hdd.serialNum + '</label></li></ul><br>';
        }
        $("#discos").html(html);
        clearInterval(interval);
    }
}

loadHdds();
interval = setInterval(loadHdds, 100);
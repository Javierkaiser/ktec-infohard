function loadMother() {
    if (computer !== null) {
        $("#motherF").text(computer.mother.manufacturer);
        $("#motherM").text(computer.mother.model);
        $("#motherV").text(computer.mother.version);
        $("#motherS").text(computer.mother.serial);
        $("#motherB").text(computer.bios.vendor);
        $("#motherBC").text(computer.bios.version);
        $("#motherBF").text(computer.bios.releaseDate);

        clearInterval(interval);
    }
}

loadMother();
interval = setInterval(loadMother, 100);
function loadGpus() {
    if (computer !== null) {
        let html = '';
        for (const gpu of computer.gpu) {
            html +=`
            <ul class = "list-group list-group-flush">
                <li class="list-group-item text-white d-flex justify-content-between align-items-center">
                    <label> Fabricante </label>
                    <label> ${gpu.vendor} </label>
                </li>
                <li class="list-group-item text-white d-flex justify-content-between align-items-center">
                    <label> Nombre </label>
                    <label> ${gpu.model} </label>
                </li>
                <li class="list-group-item text-white d-flex justify-content-between align-items-center">
                    <label> Bus </label>
                    <label> ${gpu.bus} </label>
                </li>
                <li class="list-group-item text-white d-flex justify-content-between align-items-center">
                    <label> Memoria </label>
                    <label> ${gpu.vram} </label>
                </li>
            </ul>
            <br>`;
        };
        $("#gpus").html(html);
        clearInterval(interval);
    }
}

loadGpus();
interval = setInterval(loadGpus, 100);
function loadEquipo() {
    if (computer !== null) {
        $('#mainFabricante').text(computer.sistema.manufacturer);
        $('#mainModelo').text(computer.sistema.model);
        $('#mainVersion').text(computer.sistema.version);
        $('#mainSerial').text(computer.sistema.serial);
        
        clearInterval(interval);
    }
}

loadEquipo();
interval = setInterval(loadEquipo, 100);
function loadSistema() {
    if (computer !== null) {
        $("#oscap").text(computer.os.distro);
        $("#osarq").text(computer.os.arch);
        $("#osfec").text(computer.os.codename);
        $("#osins").text(computer.os.hostname);
        $("#osdom").text(computer.os.serial);
        $("#ossta").text(computer.os.build);

        clearInterval(interval)
    }
}

loadSistema();
interval = setInterval(loadSistema, 100);
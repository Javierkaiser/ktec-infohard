function loadCpu() { 
    if (computer !== null) {
        $("#cpuF").text(computer.cpu.manufacturer);
        $("#cpuN").text(computer.cpu.brand);
        $("#cpuR").text(computer.cpu.revision);
        $("#cpuC").text(computer.cpu.physicalCores);
        $("#cpuSo").text(computer.cpu.socket);
        $("#cpuH").text(computer.cpu.cores);
        $("#cpuM").text(computer.cpu.speedmax + ' GHz');

        clearInterval(interval);
    }
}

loadCpu();
interval = setInterval(loadCpu, 100);
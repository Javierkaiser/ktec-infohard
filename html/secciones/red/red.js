function loadRed() {
    if (computer !== null) {
        let html = '';
        for (const net of computer.red) {
            const tipo = (net.type == 'wired') ? 'Cableada' : 'Inalambrica';
            html += '<ul class="list-group list-group-flush"><li class="list-group-item text-white d-flex justify-content-between align-items-center"><label> Tipo</label ><label>' + tipo + '</label></li ><li class="list-group-item text-white d-flex justify-content-between align-items-center"><label>Descripción</label><label>' + net.ifaceName + ' GB</label></li><li class="list-group-item text-white d-flex justify-content-between align-items-center"><label>MAC</label><label>' + net.mac + '</label></li><li class="list-group-item text-white d-flex justify-content-between align-items-center"><label>IP 4</label><label>' + net.ip4 + '</label></li><li class="list-group-item text-white d-flex justify-content-between align-items-center"><label>IP 6</label><label>' + net.ip6 + '</label></li></ul><br>';
        }
        $("#redes").html(html);
        clearInterval(interval);
    }
}

loadRed();
interval = setInterval(loadRed, 100);
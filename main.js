var electron = require('electron');
var {
  app,
  BrowserWindow
} = require('electron');
var ipc = electron.ipcMain
const sisinfo = require('systeminformation');

let win;

function createWindow() {
  win = new BrowserWindow({
    width: 1000,
    height: 700,
    title: "KTec InfoHard",
    autoHideMenuBar: true,
    frame: false,
    show: false,
    icon: 'imgs/icono.ico',
    webPreferences: {
      nodeIntegration: true
    }
  });
  win.loadFile('html/principal.html');
  win.maximize();
  win.once('ready-to-show', () => {
    win.show();
  });

  win.webContents.on('new-window', function (e, url) {
    e.preventDefault();
    require('electron').shell.openExternal(url);
  });
};

app.allowRendererProcessReuse = true;
app.on('ready', createWindow);

ipc.on('Minimizo', function (event, arg) {
  win.minimize();
});

ipc.on('Maximizo', function (event, arg) {
  if (win.isMaximized()) {
    win.unmaximize();
  } else win.maximize();
});

ipc.on('Cierro', function (event, arg) {
  app.quit();
});

//Envío de datos de los modulos
ipc.once("Pedido-Datos", async (e, d)=>{
  let modules = await loadModules();
  e.sender.send("Datos", modules);
});

//Cargo los Modulos
async function loadModules() {
  //Definición de objetos
  const computer = {
    cpu: '',
    gpu: '',
    mother: '',
    memoriaModulos: '',
    memoriaTotal: '',
    hdds: '',
    //sonido: '',
    red: '',
    os: '',
    sistema: '',
    bios: '',
    resumen: {
      gso: '',
      gmother: '',
      gcpu: '',
      gram: '',
      ghdds: ''
    }
  };

  //consigo los modulos
  computer.cpu = await sisinfo.cpu();
  computer.gpu = (await sisinfo.graphics()).controllers;
  computer.hdds = await sisinfo.diskLayout();
  computer.memoriaModulos = await sisinfo.memLayout();
  computer.memoriaTotal = (await sisinfo.mem()).total;
  computer.mother = await sisinfo.baseboard();
  computer.sistema = await sisinfo.system();
  computer.bios = await sisinfo.bios();
  //computer.sonido = await wmic.SoundDev();
  computer.red = await sisinfo.networkInterfaces();
  computer.os = await sisinfo.osInfo();

  //Calculo el total del disco
  let hdd = 0;
  for (let i = 0, len = computer.hdds.length; i < len; i++) {
    let preHdd = parseInt(computer.hdds[i].size, 10);
    if (!isNaN(preHdd)) {
      preHdd = Math.round(preHdd / 1073741824);
      computer.hdds[i].capacidad = preHdd;
      hdd = hdd + preHdd;
    }
  };

  //Asigno los valores principales
  computer.resumen.gso = computer.os.distro + " " + computer.os.arch;
  computer.resumen.gmother = computer.mother.manufacturer + " " + computer.mother.model;
  computer.resumen.gcpu = computer.cpu.brand;
  computer.resumen.gram = Math.round((parseInt(computer.memoriaTotal, 10) / 1073741824)) + " GB";
  computer.resumen.ghdds = hdd + " GB";
  
  return computer;
}